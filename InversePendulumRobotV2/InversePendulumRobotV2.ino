#include <PID_AutoTune_v0.h>
#include <PID_v1.h>
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"

#define MOTOR_PWMA  (5)
#define MOTOR_PWMB  (6)
#define MOTOR_AIN1  (7)
#define MOTOR_AIN2  (8)
#define MOTOR_BIN1  (9)
#define MOTOR_BIN2  (10)

#define MPU6050_SDA (2) //not used Wire library uses automatically, differs between devices
#define MPU6050_SCL (3) //not used Wire library uses automatically, differs between devices
#define MPU6050_INT (0) //This is pin for 0 or interrupt 2

#define BACKWARD    (0)
#define FORWARD     (1)

#define ENABLE      (1)
#define DISABLE     (0)

#define SETPOINT    (0)

void serialEvent();
void dmpDataReady();
int sampleYawPitchRoll(float* ypr, float* sampleTime);
void pidControl(double angle, float sampleTime, int* directionA, int* directionB, unsigned char* speedA, unsigned char* speedB);
void motorControl(int directionA, int directionB, unsigned char speedA, unsigned char speedB, int enable);

String inputString = "";         
boolean stringComplete = false;

MPU6050 mpu;
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

boolean initialize = false;
double pidInput, pidOutput, pidSetpoint = 0, pidKp = 8, pidKi = 0, pidKd = 0;
PID pid(&pidInput, &pidOutput, &pidSetpoint, pidKp, pidKi, pidKd, REVERSE);
PID_ATune aTune(&pidInput, &pidOutput);

void setup()
{
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
      TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif
  
  inputString.reserve(200);
  
  Serial.begin(38400);
  while (!Serial);
  
  Serial.println(F("Initializing I2C devices..."));
  mpu.initialize();
  
  Serial.println(F("Testing device connections..."));
  Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize(); 
  
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788);
  
  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
      // turn on the DMP, now that it's ready
      Serial.println(F("Enabling DMP..."));
      mpu.setDMPEnabled(true);

      // enable Arduino interrupt detection
      Serial.println(F("Enabling interrupt detection (Arduino external interrupt)..."));
      attachInterrupt(MPU6050_INT, dmpDataReady, RISING);
      mpuIntStatus = mpu.getIntStatus();

      // set our DMP Ready flag so the main loop() function knows it's okay to use it
      Serial.println(F("DMP ready! Waiting for first interrupt..."));
      dmpReady = true;

      // get expected DMP packet size for later comparison
      packetSize = mpu.dmpGetFIFOPacketSize();
  } 
  else {
      // ERROR!
      // 1 = initial memory load failed
      // 2 = DMP configuration updates failed
      // (if it's going to break, usually the code will be 1)
      Serial.print(F("DMP Initialization failed (code "));
      Serial.print(devStatus);
      Serial.println(F(")"));
  }
    
  //Initialize motors  
  pinMode(MOTOR_AIN1,INPUT);
  pinMode(MOTOR_AIN2, INPUT);
  pinMode(MOTOR_PWMA, OUTPUT);
  
  pinMode(MOTOR_BIN1,INPUT);
  pinMode(MOTOR_BIN2, INPUT);
  pinMode(MOTOR_PWMB, OUTPUT);
  
  digitalWrite(MOTOR_AIN1, LOW);
  digitalWrite(MOTOR_AIN2, LOW);
  analogWrite(MOTOR_PWMA, 0);
  digitalWrite(MOTOR_BIN1, LOW);
  digitalWrite(MOTOR_BIN2, LOW);
  analogWrite(MOTOR_PWMB, 0); 
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read(); 
    inputString += inChar;
    pidKi = atof(&inputString[0]);
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}

void dmpDataReady() {
    mpuInterrupt = true;
}

int sampleYawPitchRoll(float* ypr, float* sampleTime)
{
    // orientation/motion vars
    Quaternion q;           // [w, x, y, z]         quaternion container
    VectorFloat gravity;    // [x, y, z]            gravity vector
    
    while (!mpuInterrupt && fifoCount < packetSize);
    
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();
    
    fifoCount = mpu.getFIFOCount();
    
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) 
    {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));
        return 0;
    }
    else if (mpuIntStatus & 0x02)
    {
         // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;  
        
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    }
    else
    {
         return 0;
    }
    
    return 1;  
}

void pidControl(double angle, float sampleTime, int* directionA, int* directionB, unsigned char* speedA, unsigned char* speedB)
{
    pidInput = (angle > 0) ? angle : (-1*angle);
    
    pidKp = aTune.GetKp();
    pidKi = aTune.GetKi();
    pidKd = aTune.GetKd();
    
    if(!initialize)
    {
        pidSetpoint = 0;
        initialize = true;
        pid.SetMode(AUTOMATIC);
        Serial.println("initialize done!");       
    }
    else
    {
        pid.Compute();
        *speedA = *speedB = pidOutput;
        *directionA = *directionB = (angle > 0) ? 0 : 1;  
        Serial.print("PidInput: "); Serial.print(pidInput); Serial.print(" PidOutput: "); Serial.println(pidOutput);
        Serial.print(" Pid 
    }
}

void motorControl(int directionA, int directionB, unsigned char speedA, unsigned char speedB, int enable)
{
   int inA1 = 0, inA2 = 0, pwmA = 0, inB1 = 0, inB2 = 0, pwmB = 0;
   
   if(enable)
   {
       inA1 = directionA ? 0 : 1;
       inA2 = directionA ? 1 : 0;
       pwmA = (int)speedA;
       inB1 = directionB ? 0 : 1;
       inB2 = directionB ? 1 : 0;
       pwmB = (int)speedB;
   }
   
   digitalWrite(MOTOR_AIN1, inA1);
   digitalWrite(MOTOR_AIN2, inA2);
   analogWrite(MOTOR_PWMA, (unsigned char) pwmA);
   digitalWrite(MOTOR_BIN1, inB1);
   digitalWrite(MOTOR_BIN2, inB2);
   analogWrite(MOTOR_PWMB, (unsigned char) pwmB);
}

void loop()
{  
    float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
    float sampleTime = 0;
    
    //Used as a debugging tool, serialEvent is called when serial monitor sends a message
    if (Serial.available() > 0) 
    {
        serialEvent();
    }
    if (stringComplete) {
        Serial.println(inputString);   
        inputString = "";
        stringComplete = false;
    }
    
    // if programming failed, don't try to do anything
    if (!dmpReady) return;
    
    //Get reading yaw, pitch, roll from mpu6050 and get sample time
    if(sampleYawPitchRoll(&ypr[0], &sampleTime))
    {
       double angle = (double) (ypr[1] * 180/M_PI);
       int directionA = 0, directionB = 0;
       unsigned char speedA = 0, speedB = 0;
       
       /*Serial.print("ypr\t");
       Serial.print(ypr[0] * 180/M_PI);
       Serial.print("\t");
       Serial.print(ypr[1] * 180/M_PI);
       Serial.print("\t");
       Serial.println(ypr[2] * 180/M_PI);*/
       
       //Feed result to PID algorithm
       pidControl(angle, sampleTime, &directionA, &directionB, &speedA, &speedB);

       //Move motor based on result
       motorControl(directionA, directionB, speedA, speedB, ENABLE);   
    }
}


